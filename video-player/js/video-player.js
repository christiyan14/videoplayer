(function () {
	"use strict";

		// Obtain handles to buttons and other elements
		var playerVideo = "";
		var playpause = document.getElementById("playpause");
		var muteButton = document.getElementById('mute');
		var progress = document.getElementById("progress");
		var progressBar = document.getElementById("progressBAr");
		var forwardButton = document.getElementById("forward");
		var rewindButton = document.getElementById("rewind");

	// Does the browser actually support the video element?
	var supportsVideo = document.createElement("video").canPlayType;
	
	if (supportsVideo) {
		var videoContainer = document.getElementById("videoContainer");
		var videoControls = document.getElementById("videoControls");
		
		//   OBJ PLAYER
		var player = {
			loadVideo: function (src) {
				playerVideo = document.createElement("video");

				if (playerVideo.canPlayType("video/mp4")) {
					playerVideo.setAttribute("src",src);
				}

				else if (playerVideo.canPlayType("video/webm")) {
					playerVideo.setAttribute("src", "video/nature_video.webm");
				}
				else if (playerVideo.canPlayType("video/ogg")) {
					playerVideo.setAttribute("src", "video/nature_video.ogg");
				}

				playerVideo.setAttribute("width", "100%");
				playerVideo.setAttribute("height", "auto");
				playerVideo.controls = false;
				document.getElementById("videoPlayer").appendChild(playerVideo);
			},
			play: function () {
				playerVideo.play();
			},
			pause: function () {
				playerVideo.pause();
			},
			stop: function () {
				playerVideo.pause();
				playerVideo.currentTime = 0;
				progress.value = 0;
			},
			forward: function () {
				playerVideo.currentTime = playerVideo.currentTime + 10;
			},
			rewind: function () {
				playerVideo.currentTime = playerVideo.currentTime - 10;
			},
			jumpToTime: function (e) {
				var pos = (e.pageX - progress.offsetLeft) / progress.offsetWidth;
				playerVideo.currentTime = pos * playerVideo.duration;
			},
			muteVolume: function (status) {
				if(status) {
					playerVideo.muted = true;
				} else {
					playerVideo.muted = false;
				}
			},
			videoTime: false,
			isPlaying: true,
			autoplay: false,
			muteVideo: false,
		}

	// CLASS FOR PLAYER 
	class Player { 
		constructor(src,autoplay,videotime,muted) {
			this.src = src;
			this.autoplay = autoplay;
			this.videotime = videotime;
			this.muted = muted;
		}
		player() {
			player.loadVideo(this.src);
			player.autoplay = this.autoplay;
			player.videoTime = this.videotime;
			player.muted = this.muted;

				// autoplay
				if (player.autoplay) {
					playerVideo.setAttribute("autoplay", "")
				}

				//muted
				if(player.muted) {
					player.muteVolume(this.muted)
				}
			}	
		}

		// New Player: src , autoplay , videotime , mute
		var playerNew = new Player("video-source/nature_video.mp4",true,true,false);
		playerNew.player();

		// Event Listener
		if (document.addEventListener) {
			
			// Show Player current time / total time
			if (player.videoTime) {
				playerVideo.addEventListener("durationchange", function () {
					var totalMinutes = Math.floor(playerVideo.duration / 60);
					totalMinutes = ("0" + totalMinutes).slice(-2);
					var totalSeconds = Math.floor(playerVideo.duration % 60);
					totalSeconds = ("0" + totalSeconds).slice(-2);
					var totalTime = totalMinutes + ":" + totalSeconds;
					player.videoTime = "0 / 0";
					player.videoTime = player.videoTime.substring(0, player.videoTime.indexOf("/")) + "/" + totalTime;
					document.getElementById("duration").innerHTML = player.videoTime;
				});
			}

			//  Update time
			playerVideo.addEventListener("timeupdate", function () {
				var currentMinutes = Math.floor(playerVideo.currentTime / 60);
				currentMinutes = ("0" + currentMinutes).slice(-2);
				var currentSeconds = Math.floor(playerVideo.currentTime % 60);
				currentSeconds = ("0" + currentSeconds).slice(-2);
				var currentTime = currentMinutes + ":" + currentSeconds;
				player.videoTime = player.videoTime.replace(player.videoTime.split("/")[0], currentTime)
				document.getElementById("duration").innerHTML = player.videoTime;
			});

			// Forward 10 sec
			forwardButton.addEventListener("click", function (e) {
				playerVideo.currentTime = playerVideo.currentTime + 10;
			});

			// Rewind 10 sec
			rewindButton.addEventListener("click", function (e) {
				playerVideo.currentTime = playerVideo.currentTime - 10;
			});

			playerVideo.addEventListener("loadedmetadata", function () {
				progress.setAttribute("max", playerVideo.duration);
			});

			// Add events for playpause button
			playpause.addEventListener("click", function (e) {
				if (playerVideo.paused || playerVideo.ended) {
					playerVideo.play();
				} else {
					playerVideo.pause();
				}
			});

			//Update current status for isPlaying	
			playerVideo.addEventListener("play", function () {
				player.isPlaying = true;
				document.getElementById("playpause").setAttribute("data-state", "play");
			});

			playerVideo.addEventListener("pause", function () {
				player.isPlaying = false;
				document.getElementById("playpause").setAttribute("data-state", "pause");
			});
			
			// As the video is playing, update the progress bar
			playerVideo.addEventListener("timeupdate", function () {
				if (!progress.getAttribute("max")) progress.setAttribute("max", playerVideo.duration);
				progress.value = playerVideo.currentTime;
			});

			// React to the user clicking within the progress bar
			progress.addEventListener("click", function (e) {
				player.jumpToTime(e);
			});
		}
	}
})();